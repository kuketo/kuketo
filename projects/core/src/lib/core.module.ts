import { Inject, NgModule, Optional } from '@angular/core';
import { BackendService, CookiesService } from './cookies.service';
import { CookieBackend } from './interfaces';

@NgModule({
  providers: [CookiesService],
})
export class KuketoModule {
  constructor(
    @Optional() @Inject(BackendService) backendService?: CookieBackend,
  ) {
    if (!backendService) {
      throw new Error(
        'KuketoModule must be importet after or in a module providing a Backend Service (i.e. KuketoBrowserModule)',
      );
    }
  }
}

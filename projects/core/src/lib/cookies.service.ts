import { Location, PopStateEvent } from '@angular/common';
import { Injectable, Optional, Inject, InjectionToken } from '@angular/core';

import { CookieOptions, CookieBackend } from './interfaces';

export const CookieConfig = new InjectionToken<CookieOptions>('CookieOptions');
export const BackendService = new InjectionToken<CookieBackend>(
  'CookieBackend',
);

type KuketoConfig = CookieOptions &
  Required<
    Pick<CookieOptions, 'maxCookieCount' | 'maxCookieSize' | 'maxCookieSizeSum'>
  >;

@Injectable()
export class CookiesService {
  private defaultConfig: KuketoConfig = {
    maxCookieCount: Infinity,
    maxCookieSize: 4093,
    maxCookieSizeSum: Infinity,
  };
  private cookies: Map<string, string>;
  // https://tools.ietf.org/html/rfc6265#section-4.1.1
  private cookieValueRegex = /^[\x21\x23-\x2B\x2D-\x3A\x3C-\x5B\x5D-\x7E]*$/;
  // https://tools.ietf.org/html/rfc2616#section-2.2
  private cookieKeyRegex =
    /^[\x21\x23-\x27\x2A\x2B\x2D\x2E\x30-\x39\x41-\x5A\x5E-\x7A\x7C\x7E]*$/;
  private currenUrl?: string;

  constructor(
    private readonly location: Location,
    @Inject(BackendService) private readonly cookieBackend: CookieBackend,
    @Inject(CookieConfig) @Optional() defaultConfig: CookieOptions,
  ) {
    this.defaultConfig = defaultConfig
      ? { ...this.defaultConfig, ...defaultConfig }
      : this.defaultConfig;
    this.cookies = this.getCookies();
  }

  get(key: string, skipUriDecoding = false): string | undefined {
    if (this.currenUrl !== this.location.path()) {
      this.currenUrl = this.location.path();
      this.cookies = this.getCookies();
    }
    const value = this.cookies.get(key);
    return value && (skipUriDecoding ? value : decodeURIComponent(value));
  }

  put(key: string, value: string, options: CookieOptions = {}): void {
    const config = {
      ...this.defaultConfig,
      ...options,
    };
    if (!config.skipUriEncoding) {
      value = encodeURIComponent(value);
    }
    if (!this.cookieValueRegex.test(value)) {
      throw Error(`value '${value}' contains invalid characters`);
    }
    if (!this.cookieKeyRegex.test(key)) {
      throw Error(`key '${key}' contains invalid characters`);
    }
    if (this.getCookieLength(key, value) > config.maxCookieSize) {
      throw new Error(
        `Max cookie size per cookie reached (${config.maxCookieSize})`,
      );
    }
    if (
      this.rawCookieLength() + 1 + this.getCookieLength(key, value) >
      config.maxCookieSizeSum
    ) {
      throw new Error(
        `Max cookie size for all cookies reached (${config.maxCookieSizeSum})`,
      );
    }
    if (this.cookies.size >= config.maxCookieCount) {
      throw new Error('Max cookies count reached');
    }
    this.cookieBackend.writeRawCookie(key, value, config);
    this.cookies.set(key, value);
  }

  delete(key: string, options: CookieOptions = {}): void {
    const deleteOptions = {
      ...this.defaultConfig,
      ...options,
      expires: new Date(0),
    };
    delete (deleteOptions as any).maxAge;
    this.cookieBackend.writeRawCookie(key, '', deleteOptions);
    this.cookies.delete(key);
  }

  deleteByRegex(regex: RegExp, options: CookieOptions = {}): void {
    for (const key of this.cookies.keys()) {
      if (regex.test(key)) {
        this.delete(key, options);
      }
    }
  }

  private getCookies(): Map<string, string> {
    return this.cookieBackend
      .readRawCookie()
      .split(/; */)
      .filter(Boolean)
      .map((pair) => pair.split('=').map((a) => a.trim()))
      .reduce((cookiesMap, [key, value]) => {
        cookiesMap.set(key, value);
        return cookiesMap;
      }, new Map<string, string>());
  }

  private getCookieLength(key: string, value: string): number {
    return key.length + value.length + 1;
  }

  private rawCookieLength(): number {
    return Array.from(this.cookies.entries()).reduce(
      (sum, [key, value]): number => {
        return sum + this.getCookieLength(key, value);
      },
      0,
    );
  }
}

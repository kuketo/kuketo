import { TestBed } from '@angular/core/testing';
import { KuketoModule } from './core.module';
import { BackendService, CookiesService } from './cookies.service';

describe('KuketoModule', () => {
  describe('importing directly w/o BackendService', () => {
    it('fails', () => {
      TestBed.configureTestingModule({
        imports: [KuketoModule],
      });
      expect(() => {
        TestBed.inject(CookiesService);
      }).toThrowError(
        'KuketoModule must be importet after or in a module providing a Backend Service (i.e. KuketoBrowserModule)',
      );
    });
  });

  describe('imported when BackendService is provided', () => {
    class TestinBackendService {
      readRawCookie() {
        return '';
      }
    }
    it('succeeds', () => {
      TestBed.configureTestingModule({
        imports: [KuketoModule],
        providers: [
          { provide: BackendService, useClass: TestinBackendService },
        ],
      });
      expect(() => {
        TestBed.inject(CookiesService);
      }).not.toThrow();
    });
  });
});

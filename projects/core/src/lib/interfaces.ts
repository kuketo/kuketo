import type { CookieOptions as ExpressCookieOptions } from 'express';

export enum SameSite {
  Lax = 'lax',
  Strict = 'strict',
  None = 'none',
}

export interface CookieOptions extends ExpressCookieOptions {
  sameSite?: SameSite;
  skipUriEncoding?: boolean;
  /**
   * maximum size per cookie in bytes
   */
  maxCookieSize?: number;
  /**
   * maximum of all cookies in bytes
   */
  maxCookieSizeSum?: number;
  /**
   * maximum amount of cookies
   */
  maxCookieCount?: number;
}

export interface CookieBackend {
  readRawCookie(): string;
  writeRawCookie(key: string, value: string, options: CookieOptions): void;
}

import { Location } from '@angular/common';
import { Injectable, OnDestroy, Optional } from '@angular/core';
import { CookieBackend, CookieOptions, SameSite } from '@kuketo/core';

interface Cookie {
  domain?: string;
  path?: string;
  secure?: boolean;
  sameSite?: SameSite;
  maxAge?: number;
  value: string;
}

@Injectable()
export class CookieStoreService implements CookieBackend, OnDestroy {
  private cookies = new Map<string, Cookie>();
  private timers = new Map<string, number>();

  constructor(@Optional() private readonly location: Location) {}

  clear() {
    for (const timer of this.timers.values()) {
      clearTimeout(timer);
    }
    this.timers.clear();
  }

  getCookie(key: string) {
    return this.cookies.get(key);
  }

  readRawCookie() {
    const path = this.location?.path() ?? '/';
    return (
      Array.from(this.cookies.entries())
        // not 100% correct since path seperators are ignored
        // TODO: fix path seperator issue
        .filter(([_, cookie]) => path.startsWith(cookie.path ?? ''))
        .map(([key, cookie]) => `${key.split('@')[0]}=${cookie.value}`)
        .join('; ')
    );
  }

  writeRawCookie(
    originalKey: string,
    value: string,
    options: CookieOptions,
  ): void {
    const key = `${originalKey}@${options.path}`;
    this.cookies.set(key, {
      value,
      path: options.path,
      domain: options.domain,
      secure: options.secure,
      sameSite: options.sameSite,
      maxAge:
        options.maxAge ??
        (options.expires
          ? Math.floor((+options.expires - Date.now()) / 1000)
          : undefined),
    });
    const maxAge = this.cookies.get(key)?.maxAge;
    if (maxAge) {
      this.timers.set(
        key,
        setTimeout(() => this.cookies.delete(key), maxAge * 1000) as any,
      );
    }
  }

  ngOnDestroy() {
    this.clear();
  }
}

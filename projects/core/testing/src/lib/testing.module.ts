import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf,
} from '@angular/core';
import {
  KuketoModule,
  CookieOptions,
  CookieConfig,
  BackendService,
} from '../../../src/public-api';
import { CookieStoreService } from './cookie-store.service';

@NgModule({
  imports: [KuketoModule],
  providers: [
    CookieStoreService,
    { provide: BackendService, useClass: CookieStoreService },
  ],
})
export class KuketoTestingModule {
  constructor(@Optional() @SkipSelf() parentModule?: KuketoTestingModule) {
    if (parentModule) {
      throw new Error(
        'KuketoTestingModule is already loaded. Import it only once',
      );
    }
  }

  static withConfig(
    config: CookieOptions,
  ): ModuleWithProviders<KuketoTestingModule> {
    return {
      ngModule: KuketoTestingModule,
      providers: [
        { provide: CookieConfig, useValue: config },
        { provide: BackendService, useClass: CookieStoreService },
        CookieStoreService,
      ],
    };
  }
}

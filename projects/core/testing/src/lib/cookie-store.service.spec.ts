import { Location } from '@angular/common';
import { SpyLocation } from '@angular/common/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { CookieStoreService } from './cookie-store.service';

describe('CookieStoreService', () => {
  describe('w/o router', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        providers: [CookieStoreService],
      });
    });

    let service: CookieStoreService;
    beforeEach(() => {
      service = TestBed.inject(CookieStoreService);
    });

    it('provides CookieStoreService', () => {
      expect(service).toBeTruthy();
    });

    it('stores cookies', () => {
      service.writeRawCookie('key', 'string', {});
      expect(service.readRawCookie()).toEqual('key=string');
    });

    it('deletes cookies after maxAge', fakeAsync(() => {
      service.writeRawCookie('key', 'string', { maxAge: 1 });
      expect(service.readRawCookie()).toEqual('key=string');
      tick(1000);
      expect(service.readRawCookie()).toEqual('');
    }));
  });

  describe('with Router', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        providers: [
          CookieStoreService,
          { provide: Location, useClass: SpyLocation },
        ],
      });
    });

    it('provides CookieStoreService', () => {
      expect(TestBed.inject(CookieStoreService)).toBeTruthy();
    });

    let location: Location;
    let service: CookieStoreService;
    beforeEach(() => {
      service = TestBed.inject(CookieStoreService);
      location = TestBed.inject(Location);
    });

    it('only provides cookies accessible by route', () => {
      service.writeRawCookie('cookie1', 'value1', { path: '/' });
      service.writeRawCookie('cookie2', 'value2', { path: '/path' });

      location.go('/');
      expect(service.readRawCookie()).toMatch('cookie1=value1');
      expect(service.readRawCookie()).not.toMatch('cookie2=value2');

      location.go('/path');
      expect(service.readRawCookie()).toMatch('cookie1=value1');
      expect(service.readRawCookie()).toMatch('cookie2=value2');
    });
  });
});

import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf,
} from '@angular/core';
import {
  KuketoModule,
  BackendService,
  CookieOptions,
  CookieConfig,
} from '@kuketo/core';
import { ExpressBackendService } from './express-backend.service';

@NgModule({
  imports: [KuketoModule],
  providers: [{ provide: BackendService, useClass: ExpressBackendService }],
})
export class KuketoExpressModule {
  constructor(@Optional() @SkipSelf() parentModule?: KuketoExpressModule) {
    if (parentModule) {
      throw new Error(
        'KuketoExpressModule is already loaded. Import it in the AppModule only',
      );
    }
  }

  static withConfig(
    config: CookieOptions,
  ): ModuleWithProviders<KuketoExpressModule> {
    return {
      ngModule: KuketoExpressModule,
      providers: [
        { provide: CookieConfig, useValue: config },
        {
          provide: BackendService,
          useClass: ExpressBackendService,
        },
      ],
    };
  }
}

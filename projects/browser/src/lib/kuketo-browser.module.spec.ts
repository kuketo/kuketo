import { TestBed } from '@angular/core/testing';
import {
  BackendService,
  CookieConfig,
  CookiesService,
  CookieOptions,
} from '@kuketo/core';
import { BrowserBackendService } from './browser-backend.service';
import { KuketoBrowserModule } from './kuketo-browser.module';

describe('KuketoBrowserModule', () => {
  describe('without config', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [KuketoBrowserModule],
      });
    });

    it('provides CookieService', () => {
      expect(TestBed.inject(CookiesService)).toBeTruthy();
    });

    it('provides CookieHandlerService', () => {
      expect(TestBed.inject(BackendService)).toBeTruthy();
      expect(TestBed.inject(BackendService)).toBeInstanceOf(
        BrowserBackendService,
      );
    });

    it('does not provide CookieConfig', () => {
      expect(() => TestBed.inject(CookieConfig)).toThrowError();
    });
  });

  describe('with config', () => {
    const config: CookieOptions = { path: '/' };
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [KuketoBrowserModule.withConfig({ path: '/' })],
      });
    });

    it('provides CookieService', () => {
      expect(TestBed.inject(CookiesService)).toBeTruthy();
    });

    it('provides CookieHandlerService', () => {
      expect(TestBed.inject(BackendService)).toBeTruthy();
      expect(TestBed.inject(BackendService)).toBeInstanceOf(
        BrowserBackendService,
      );
    });

    it('does not provide CookieConfig', () => {
      expect(TestBed.inject(CookieConfig)).toEqual(config);
    });
  });
});

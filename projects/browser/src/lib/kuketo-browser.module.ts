import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf,
} from '@angular/core';
import {
  KuketoModule,
  BackendService,
  CookieOptions,
  CookieConfig,
} from '@kuketo/core';
import { BrowserBackendService } from './browser-backend.service';

@NgModule({
  imports: [KuketoModule],
  providers: [{ provide: BackendService, useClass: BrowserBackendService }],
})
export class KuketoBrowserModule {
  constructor(@Optional() @SkipSelf() parentModule?: KuketoBrowserModule) {
    if (parentModule) {
      throw new Error(
        'KuketoBrowserModule is already loaded. Import it in the AppModule only',
      );
    }
  }

  static withConfig(
    config: CookieOptions,
  ): ModuleWithProviders<KuketoBrowserModule> {
    return {
      ngModule: KuketoBrowserModule,
      providers: [
        { provide: CookieConfig, useValue: config },
        {
          provide: BackendService,
          useClass: BrowserBackendService,
        },
      ],
    };
  }
}

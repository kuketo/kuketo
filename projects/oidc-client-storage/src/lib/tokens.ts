import { InjectionToken } from '@angular/core';
import { OidcCookieConfig, OidcCookieSerializer } from './interfaces';

export const OIDC_COOKIE_SERIALIZER = new InjectionToken<OidcCookieSerializer>(
  'OIDC_COOKIE_SERIALIZER',
);

export const OIDC_COOKIE_CONFIG = new InjectionToken<OidcCookieConfig>(
  'OIDC_COOKIE_CONFIG',
);

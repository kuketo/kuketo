export interface OidcCookieConfig {
  ttl: number;
  path: string;
  name: string;
}

export interface OidcCookieSerializer {
  stringify<T>(value: T): string;
  parse<T>(value: string): T;
}

import { APP_BASE_HREF } from '@angular/common';
import { ModuleWithProviders, NgModule, Optional } from '@angular/core';
import { KuketoModule } from '@kuketo/core';
import { OidcCookieConfig } from './interfaces';
import { OidcClientStorageService } from './oidc-client-storage.service';
import { OIDC_COOKIE_CONFIG } from './tokens';

function configFactory(
  baseHref: string,
  config: Partial<OidcCookieConfig> = {},
): OidcCookieConfig {
  return { ttl: 3600, path: baseHref, name: '%oidc%', ...config };
}

@NgModule({
  providers: [
    {
      provide: OIDC_COOKIE_CONFIG,
      useFactory: configFactory,
      deps: [APP_BASE_HREF],
    },
    OidcClientStorageService,
  ],
})
export class OidcClientStorageModule {
  constructor(@Optional() parentModule: KuketoModule) {
    if (!parentModule) {
      throw new Error('KuketoModule is required for this module to work');
    }
  }

  static withConfig(
    config: Partial<OidcCookieConfig>,
  ): ModuleWithProviders<OidcClientStorageModule> {
    return {
      ngModule: OidcClientStorageModule,
      providers: [
        {
          provide: OIDC_COOKIE_CONFIG,
          useFactory(baseHref: string): OidcCookieConfig {
            return configFactory(baseHref, config);
          },
          deps: [APP_BASE_HREF],
        },
        OidcClientStorageService,
      ],
    };
  }
}

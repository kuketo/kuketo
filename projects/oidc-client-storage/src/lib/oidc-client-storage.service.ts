import { APP_BASE_HREF } from '@angular/common';
import { Inject, Injectable, Optional } from '@angular/core';
import { CookiesService, CookieOptions } from '@kuketo/core';
import { AbstractSecurityStorage } from 'angular-auth-oidc-client';
import { OidcCookieSerializer } from './interfaces';
import { OIDC_COOKIE_SERIALIZER } from './tokens';

@Injectable()
export class OidcClientStorageService extends AbstractSecurityStorage {
  constructor(
    private readonly cookieService: CookiesService,
    @Inject(APP_BASE_HREF) private readonly baseHref: string,
    @Optional()
    @Inject(OIDC_COOKIE_SERIALIZER)
    private readonly serializer: OidcCookieSerializer,
  ) {
    super();
    this.serializer ||= JSON;
  }

  read<T>(key: string, configId?: string): T | undefined {
    const name: string = this.getPrefix(key, configId);
    const value = this.cookieService.get(name);
    return !!value ? this.serializer.parse<T>(value) : undefined;
  }

  write<T>(key: string, value: T, configId?: string): void {
    this.cookieService.put(
      this.getPrefix(key, configId),
      this.serializer.stringify<T>(value),
      this.getCookieOptions(),
    );
  }

  remove(key: string, configId?: string): void {
    this.cookieService.delete(
      this.getPrefix(key, configId),
      this.getCookieOptions(),
    );
  }

  clear(configId: string) {
    const regex = new RegExp(`^${this.getPrefix('', configId)}`);
    this.cookieService.deleteByRegex(regex, this.getCookieOptions());
  }

  private getPrefix(key: string, configId?: string): string {
    return `%oidc[${configId ?? ''}]%${key}`;
  }

  private getCookieOptions(): CookieOptions {
    return {
      path: this.baseHref,
    };
  }
}

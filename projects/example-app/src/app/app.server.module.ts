import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { KuketoExpressModule } from '@kuketo/express';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';

@NgModule({
  imports: [AppModule, ServerModule, KuketoExpressModule],
  bootstrap: [AppComponent],
})
export class AppServerModule {}

import { Component, OnInit } from '@angular/core';
import { CookiesService } from '@kuketo/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'example-app';
  constructor(private readonly cookieService: CookiesService) {}

  ngOnInit() {
    this.cookieService.put('first', 'firstValue');
    this.cookieService.delete('second');
  }
}

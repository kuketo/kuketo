import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { KuketoBrowserModule } from '@kuketo/browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    KuketoBrowserModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
